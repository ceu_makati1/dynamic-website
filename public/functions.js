document.getElementById("beringer").addEventListener('click', beringer);
document.getElementById("buyB").addEventListener('click', BuyBeringer);
document.getElementById("Sottimano").addEventListener('click', Sottimano);
document.getElementById("buyS").addEventListener('click', BuySottimano);
document.getElementById("bogle").addEventListener('click', bogle);
document.getElementById("buyBo").addEventListener('click', BuyBogle);
document.getElementById("Giesen").addEventListener('click', Giesen);
document.getElementById("buyG").addEventListener('click', BuyGiesen);
document.getElementById("Cabernet").addEventListener('click', Cabernet);
document.getElementById("buyC").addEventListener('click', BuyCabernet);
document.getElementById("Weingut").addEventListener('click', Weingut);
document.getElementById("buyW").addEventListener('click', BuyWeingut);
document.getElementById("Prosecco").addEventListener('click', Prosecco);
document.getElementById("buyP").addEventListener('click', BuyProsecco);
document.getElementById("Alois").addEventListener('click', Alois);
document.getElementById("buyA").addEventListener('click', BuyAlois);
document.getElementById("cart").addEventListener('click', AddtoCart);
document.getElementById("buy").addEventListener('click', BuyNow);

function cash(){
    document.getElementById('method').innerHTML = "CASH"
}
function credit(){
    document.getElementById('method').innerHTML = "CREDIT/DEBIT CARD"
}

function checkout(){
    let fname = document.getElementById('f').value;
    let lname = document.getElementById('l').value;
    let email = document.getElementById('e').value;
    let phone = document.getElementById('p').value;
    let address = document.getElementById('ba').value;
    let pay = document.getElementById('method').textContent;

    document.getElementById('fname').innerHTML = fname;
    document.getElementById('lname').innerHTML = lname;
    document.getElementById('email').innerHTML = email;
    document.getElementById('pnum').innerHTML = phone;
    document.getElementById('address').innerHTML = address;
    document.getElementById('payment').innerHTML = pay;
}
function remove(obj){
    var ch = confirm("are you sure you want to remove this product?");
    if(ch == true){
    var index = obj.parentNode.parentNode.parentNode.rowIndex;
    var table = document.getElementById("cartss");
    table.deleteRow(index);
    }else{
        alert("no rows affected");
    }
}

function AddtoCart(){
   let quantity = Number(document.getElementById('ilan').value);

    if(quantity != 0){
        
        alert('Successfully added!');
    }else
        alert('No product added');
}

function BuyNow(){
    let quantity = Number(document.getElementById('howmany').value);

    if(quantity != 0){
        alert('Successfully added!');
    }else
        alert('No product added');

}

function AloisQ(){
    let price = Number(document.getElementById('productPriceA').textContent);
    let quantity = Number(document.getElementById('cartA').value);
    let total = price * quantity;
    document.querySelector('#subTA').innerHTML = total;
}

function GiesenQ(){
    let price = Number(document.getElementById('productPriceG').textContent);
    let quantity = Number(document.getElementById('cartG').value);
    let total = price * quantity;
    document.querySelector('#subTG').innerHTML = total;
}

function BeringerQ(){
    let price = Number(document.getElementById('productPriceB').textContent);
    let quantity = Number(document.getElementById('cartQ').value);
    let total = price * quantity;
    document.querySelector('#subTB').innerHTML = total;
}

function beringer(){
    document.getElementById("ilan").addEventListener('change', addB);
    let name = document.getElementById('nameB').textContent;
    let price = Number(document.getElementById('priceB').textContent);
    document.querySelector('.modal-title').innerHTML = name+' - '+ 'P'+price;

    function addB(){
        let price = Number(document.getElementById('priceB').textContent);
        let quantity = Number(document.getElementById('ilan').value);
        let total = price * quantity;
        document.querySelector('#total').innerHTML = total;
    }

    addB();
    
}

function BuyBeringer(){

    document.getElementById("howmany").addEventListener('change', add);
    let name = document.getElementById('nameB').textContent;
    let price = Number(document.getElementById('priceB').textContent);
    document.querySelector('#titleB').innerHTML = name+' - '+ 'P'+price;

    function add(){
        let price = Number(document.getElementById('priceB').textContent);
        let quantity = Number(document.getElementById('howmany').value);
        let total = price * quantity;
        document.querySelector('#totalB').innerHTML = total;
    }
   
    add();
}

function Sottimano(){
    document.getElementById("ilan").addEventListener('change', addS);
    let name = document.getElementById('nameS').textContent;
    let price = document.getElementById('priceS').textContent;
    document.querySelector('.modal-title').innerHTML = name+' - '+ 'P'+price;

    function addS(){
        let price = Number(document.getElementById('priceS').textContent);
        let quantity = Number(document.getElementById('ilan').value);
        let total = price * quantity;
        document.querySelector('#total').innerHTML = total;
    }
    addS();
}
function BuySottimano(){
    document.getElementById("howmany").addEventListener('change', addS);
    let name = document.getElementById('nameS').textContent;
    let price = document.getElementById('priceS').textContent;
    document.querySelector('#titleB').innerHTML = name+' - '+ 'P'+price;

    function addS(){
        let price = Number(document.getElementById('priceS').textContent);
        let quantity = Number(document.getElementById('howmany').value);
        let total = price * quantity;
        document.querySelector('#totalB').innerHTML = total;
    }
    addS();
}

function bogle(){
    document.getElementById("ilan").addEventListener('change', addBo);
    let name = document.getElementById('nameBo').textContent;
    let price = document.getElementById('priceBo').textContent;
    document.querySelector('.modal-title').innerHTML = name+' - '+ 'P'+price;

    function addBo(){
        let price = Number(document.getElementById('priceBo').textContent);
        let quantity = Number(document.getElementById('ilan').value);
        let total = price * quantity;
        document.querySelector('#total').innerHTML = total;
    }
    addBo();
}
function BuyBogle(){
    document.getElementById("howmany").addEventListener('change', add);
    let name = document.getElementById('nameBo').textContent;
    let price = document.getElementById('priceBo').textContent;
    document.querySelector('#titleB').innerHTML = name+' - '+ 'P'+price;

    function add(){
        let price = Number(document.getElementById('priceBo').textContent);
        let quantity = Number(document.getElementById('howmany').value);
        let total = price * quantity;
        document.querySelector('#totalB').innerHTML = total;
    }
    add();
}

function Giesen(){
    document.getElementById("ilan").addEventListener('change', addG);
    let name = document.getElementById('nameG').textContent;
    let price = document.getElementById('priceG').textContent;
    document.querySelector('.modal-title').innerHTML = name+' - '+ 'P'+price;

    function addG(){
        let price = Number(document.getElementById('priceG').textContent);
        let quantity = Number(document.getElementById('ilan').value);
        let total = price * quantity;
        document.querySelector('#total').innerHTML = total;
    }
    addG();
}
function BuyGiesen(){
    document.getElementById("howmany").addEventListener('change', add);
    let name = document.getElementById('nameG').textContent;
    let price = document.getElementById('priceG').textContent;
    document.querySelector('#titleB').innerHTML = name+' - '+ 'P'+price;

    function add(){
        let price = Number(document.getElementById('priceG').textContent);
        let quantity = Number(document.getElementById('howmany').value);
        let total = price * quantity;
        document.querySelector('#totalB').innerHTML = total;
    }
    add();
}

function Cabernet(){
    document.getElementById("ilan").addEventListener('change', addC);
    let name = document.getElementById('nameC').textContent;
    let price = document.getElementById('priceC').textContent;
    document.querySelector('.modal-title').innerHTML = name+' - '+ 'P'+price;

    function addC(){
        let price = Number(document.getElementById('priceC').textContent);
        let quantity = Number(document.getElementById('ilan').value);
        let total = price * quantity;
        document.querySelector('#total').innerHTML = total;
    }
    addC();
}
function BuyCabernet(){
    document.getElementById("howmany").addEventListener('change', addC);
    let name = document.getElementById('nameC').textContent;
    let price = document.getElementById('priceC').textContent;
    document.querySelector('#titleB').innerHTML = name+' - '+ 'P'+price;

    function addC(){
        let price = Number(document.getElementById('priceC').textContent);
        let quantity = Number(document.getElementById('howmany').value);
        let total = price * quantity;
        document.querySelector('#totalB').innerHTML = total;
    }
    addC();
}

function Weingut(){
    document.getElementById("ilan").addEventListener('change', addW);
    let name = document.getElementById('nameW').textContent;
    let price = document.getElementById('priceW').textContent;
    document.querySelector('.modal-title').innerHTML = name+' - '+ 'P'+price;

    function addW(){
        let price = Number(document.getElementById('priceW').textContent);
        let quantity = Number(document.getElementById('ilan').value);
        let total = price * quantity;
        document.querySelector('#total').innerHTML = total;
    }
    addW();
}
function BuyWeingut(){
    document.getElementById("howmany").addEventListener('change', addW);
    let name = document.getElementById('nameW').textContent;
    let price = document.getElementById('priceW').textContent;
    document.querySelector('#titleB').innerHTML = name+' - '+ 'P'+price;

    function addW(){
        let price = Number(document.getElementById('priceW').textContent);
        let quantity = Number(document.getElementById('howmany').value);
        let total = price * quantity;
        document.querySelector('#totalB').innerHTML = total;
    }
    addW();
}

function Prosecco(){
    document.getElementById("ilan").addEventListener('change', addP);
    let name = document.getElementById('nameP').textContent;
    let price = document.getElementById('priceP').textContent;
    document.querySelector('.modal-title').innerHTML = name+' - '+ 'P'+price;

    function addP(){
        let price = Number(document.getElementById('priceP').textContent);
        let quantity = Number(document.getElementById('ilan').value);
        let total = price * quantity;
        document.querySelector('#total').innerHTML = total;
    }
    addP();
}
function BuyProsecco(){
    document.getElementById("howmany").addEventListener('change', addP);
    let name = document.getElementById('nameP').textContent;
    let price = document.getElementById('priceP').textContent;
    document.querySelector('#titleB').innerHTML = name+' - '+ 'P'+price;

    function addP(){
        let price = Number(document.getElementById('priceP').textContent);
        let quantity = Number(document.getElementById('howmany').value);
        let total = price * quantity;
        document.querySelector('#totalB').innerHTML = total;
    }
    addP();
}

function Alois(){
    document.getElementById("ilan").addEventListener('change', addA)
    let name = document.getElementById('nameA').textContent;
    let price = document.getElementById('priceA').textContent;
    document.querySelector('.modal-title').innerHTML = name+' - '+ 'P'+price;

    function addA(){
        let price = Number(document.getElementById('priceA').textContent);
        let quantity = Number(document.getElementById('ilan').value);
        let total = price * quantity;
        document.querySelector('#total').innerHTML = total;
    }
    addA();
}
function BuyAlois(){
    document.getElementById("howmany").addEventListener('change', addA)
    let name = document.getElementById('nameA').textContent;
    let price = document.getElementById('priceA').textContent;
    document.querySelector('#titleB').innerHTML = name+' - '+ 'P'+price;

    function addA(){
        let price = Number(document.getElementById('priceA').textContent);
        let quantity = Number(document.getElementById('howmany').value);
        let total = price * quantity;
        document.querySelector('#totalB').innerHTML = total;
    }
    addA();
}